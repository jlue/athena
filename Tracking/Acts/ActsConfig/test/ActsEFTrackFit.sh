#!/usr/bin/bash
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# run the example script
python3 -m ActsConfig.ActsProtoTrackCreationAndFitConfig Exec.FPE=-1
# python3 -m ActsConfig.ActsProtoTrackCreationAndFitConfig Exec.DebugStage="'exec'"  - to start debugger
# python3 -m ActsConfig.ActsProtoTrackCreationAndFitConfig Input.Files="['location']" - to change input file
# python3 -m ActsConfig.ActsProtoTrackCreationAndFitConfig  outputNTupleFile="rr.root"  - to set output ntuple:
# or any other flags or combinations of those
