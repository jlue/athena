/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../RootCollection.h"
using pool::RootCollection::RootCollection;
DECLARE_COMPONENT_WITH_ID(RootCollection, "RootCollection")

#include "../RNTCollection.h"
using pool::RootCollection::RNTCollection;
DECLARE_COMPONENT_WITH_ID(RNTCollection, "RNTCollection")
